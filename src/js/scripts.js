(function ($, window, document, undefined) {

  'use strict';

  $(function () {

    var HEADERS = {
      'Client-ID': '19no0fs39lcw2zaa2zvy777p8xjhtr',
      'accept': 'application/vnd.twitchtv.v5+json'
    }

    function getStreamsFromCommunity() {
      return $.ajax({
        method: 'GET',
        url: 'https://api.twitch.tv/helix/streams?community_id=e0baaea8-db7a-4415-a2b9-bef6af1a8ac7',
        headers: HEADERS
      }).then(function(data) {
        return data.data;
      })
    }

    function getUserById(id) {
      return $.ajax({
        method: 'GET',
        url: 'https://api.twitch.tv/kraken/users/' + id,
        headers: HEADERS
      })
    }
    function getUsersByIds(ids) {
      var promises = [];
      ids.forEach(function(id) {
        promises.push(getUserById(id))
      });

      return $.when.apply(this, promises).then(function() {
        var users = [];
        [].forEach.call(arguments, function (user) {
          users.push(user[0])
        });

        return users;
      })


    }


    function makeStreamIframe(user) {
      var wrapper = $('<div class="stream-wrapper"></div>')
      var iframe = $('<iframe></iframe>');

      var chatBtn = $('<span class="chat-button">Open Chat</span>')
      wrapper.addClass('channel-' + user);
      wrapper.append(iframe);
      wrapper.append(chatBtn);
      var url = 'http://player.twitch.tv/?channel=' + user;
      iframe.attr('src', url); 

      return wrapper;
    }


    function makeChatIframe(user, display_name) {
      var wrapper = $('<div class="chat-wrapper"></div>')
      var iframe = $('<iframe></iframe>');
      var h2 = $('<h2>' + display_name + '</h2>')
      wrapper.append(h2);
      wrapper.append(iframe);
      wrapper.addClass('channel-'+ user);
      var url = 'http://www.twitch.tv/embed/' + user + '/chat';
      iframe.attr('src', url); 
      return wrapper;
    }

    function selectStream(channelName) {
      $('.channel-' + channelName)
        .addClass('selected')
        .siblings()
        .removeClass('selected');
    }


    // initialize 

    var existing_users = [];
    var initialized = false;
    function initialize() {
      getStreamsFromCommunity().then(function(streams) {
        var ids = [];

        streams.forEach(function(stream) {
          ids.push(stream.user_id);
        });

        getUsersByIds(ids).then(function(users) {
          var new_users = _.map(users, 'name');
          console.log(new_users)
          if (existing_users.length === 0) {
            existing_users = new_users;
          } else {
            var offline_users = _.difference(new_users, existing_users);
            offline_users.forEach(function(user) {
              $('.stream-wrapper.channel-' + user).remove();
            })
          }

          existing_users.forEach(function(user) {
            console.log('.channel-' + user);
            console.log($('#streams .channel-' + user).length);
            if ($('#streams .channel-' + user).length === 0) {
              $('#streams').append(makeStreamIframe(user));
            }
            if ($('#chat .channel-' + user).length === 0) {
              // console.log(users.find({ name: user }).display_name)
              $('#chat').append(makeChatIframe(user, _.find(users, { name: user }).display_name ));
            }
          });

          if (!initialized) {
            $('.chat-wrapper').first().addClass('selected')
            $('.stream-wrapper').first().addClass('selected')
          }

          initialized = true;
          setTimeout(initialize, 15000);
        });
        
      })



    }

    initialize();

    // events
    $('#streams').on('click', '.chat-button', function(event) {
      var btn = event.currentTarget;
      var classes = btn.parentElement.classList.value;
      var regex = /channel-(.+)/;
      var matches = regex.exec(classes);
      var channelName = matches[1];
      selectStream(channelName);
    })
  });

})(jQuery, window, document);
